"""
Fitxer amb diverses definicions per generar combinacions d'horaris.

29/1/2017
(C) 2017 Eloi Codina
"""

class HourBusy(Exception):
    """ Error que s'aixeca quan s'intenta afegir una assignatura amb solapaments.
    """
    pass

class GroupNotValid(Exception):
    """ Error que s'aixeca quan s'intenta afegir un grup de pràctiques que no
        correspon al grup de teoria. T:10 --> P:11,12,13,14,15,16,17,18 o 19
    """
    pass

def generateHours():
    """ Generador d'hores, de les 8:00 a les 19:00, intervals de 30 minuts
    Yields:
        int : hora en format 800/830/900/930....
    """
    i = 0
    c = 800
    while c != 1930:
        yield c
        c = c+30 if i%2 == 0 else c+70
        i += 1
        
def generateHoursFrom(hour):
    """ Generador d'hores, a partir de `hour`, intervals de 30 minuts
    Args:
        hour (str/int) : Hora a partir de la qual es vol generar hores. Ex: 800
    Yields:
        int : hora en format 800/830/900...
    """
    i = 1 if str(hour)[-2] == '3' else 0
    while True:
        yield hour
        hour = hour+30 if i%2 == 0 else hour+70
        i += 1

def beautifyHour(hour):
    """ Retorna una hora llegible.
    Args:
        hour (str/int) : hora en format 800/1000/2030
    Returns:
        str : hora llegible en format 8:00/10:30
    """
    hour = str(hour)
    return hour[:-2]+':'+hour[-2:]
        


class TimeTable:
    """ Classe que emmagatzema un horari """

    def __init__(self):
        # En aquesta variable es guardaran les assignatures a l'hora que toca
        # A les hores ocupades s'emmagatzema un tuple de forma (número grup, nom assignatura, color taula HTML)
        self.data = {1: {},
                     2: {},
                     3: {},
                     4: {},
                     5: {}}
        self.subjects = {}
        
    def __getitem__(self, pos):
        """ Retorna l'assignatura, si n'hi ha de la posició `pos`
        Args:
            pos (tuple) : dia de la setmana i hora que es vol veure. Ex: (5,1200) == (dia, hora)
        Returns:
            tuple / None : tuple amb (grup, assignatura, color per la taula html) o None si no hi ha res
        Ús:
            self[(dia, hora)]
            1:dilluns
            ...
            5:divendres
        """
        if pos[1] in self.data[pos[0]]:
            return self.data[pos[0]][pos[1]]

    def __setitem__(self, pos, new):
        """ Afegeix una assignatura a la posició `pos`
        Args:
            pos (tuple) : dia de la setmana i hora que es vol veure. Ex: (5,1200) == (dia, hora)
            new (tuple) : informació de l'assignatura. (grup, assignatura, color per la taula html)
        Ús:
            self[(dia, hora)] = new
            1:dilluns
            ...
            5:divendres
        """
        self.data[pos[0]][pos[1]] = new

    def __str__(self):
        """ Mostra una taula HTML amb tota la informació emmagatzmada a `self`
        Returns:
            str : taula HTML
        Ús:
            str(self)
        """
        dow = ['', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres']
        s= '<table border=1 style="width:100%"><tr><td bgcolor="LightGrey">'+ '</td><td bgcolor="LightGrey" align="center"><b>'.join(dow)+'</b></td></tr>'
        h = generateHours()
        for fila in h:
            i = generateHoursFrom(fila)
            next(i)
            t=['<td bgcolor="LightGrey" align="center"><b>'+beautifyHour(fila)+'-'+beautifyHour(next(i))+'</b></td>']
            for columna in range(1,6):
                if self.isBusy(columna, fila):
                    b = self[columna, fila]
                    t.append('<td bgcolor="'+b[2]+'" align="center">G'+str(b[0])+', '+b[1]+'</td>')
                else:
                    t.append('<td></td>')
            s = s + '<tr>'+''.join(t)+'</tr>'
        s += '</table>'
        return s
    
    def isBusy(self, day, hour):
        """ Retorna True si l'hora ja està ocupada
        Args:
            day (int) : dia de la setmana (1: dilluns, ..., 5: divendres)
            hour (int) : hora del dia: 800 / 1000 / 1030 (de mitja hora en mitja hora)
        Returns:
            bool : True si està ocupat, False altrament
        """
        return self[(day,hour)] is not None

    def checkBusy(self, day, hours):
        """ Aixeca l'error `HourBusy` si alguna de les hores ja està ocupada
        Args:
            day (int) : dia de la setmana (1: dilluns, ..., 5: divendres)
            hours (list) : hores que es vol consultar: [inici,final] == [800,1200] == 8:00,8:30,9:00,9:30,...,12:00
        """
        h = generateHoursFrom(hours[0])
        ch = hours[0]
        while ch != hours[1]:
            ch = next(h)
            if self.isBusy(day, ch):
                raise HourBusy
    
    def checkBusySeveral(self, time):
        """ Aixeca l'error `HourBusy` si alguna de les hores ja està ocupada
        Args:
            time (list) : llista de dies i hores que es vol consultar:
                        [(dia setmana 1,[hora inici 1, hora final 2]), (ds2, [hi2,hf2])]
        """
        for t in time:
            self.checkBusy(t[0],t[1])

    def isValid(self, code, typ, group):
        """ Aixeca l'error `GroupNotValid` si alguna de les hores ja està ocupada
        Args:
            code (int) : codi de l'assignatura. Ex 240041
            typ (str) : tipus d'assignatura. Teoria: 't', Pràctiques: 'p'
            group (int) : número del grup que es vol afegir
        """
        if typ == 'p' and str(self.subjects.get(code,group))[0] != str(group)[0]:
            raise GroupNotValid
        self.subjects[code] = group

    def loadSubject(self, subject, code, typ, group, time, color):
        """ Afegeix una assignatura a l'horari
        Args:
            subject (str) : nom de l'assignatura
            code (int) : codi de l'assignatura. Ex 240041
            typ (str) : tipus d'assignatura. Teoria: 't', Pràctiques: 'p'
            group (int) : número del grup
            time (list) : dies i hores. Ex: 
                [(dia de la setmana 1, [hora inici 1, hora final 1]), (ds2, [hi2, hf2])]
            color (str) : nom d'un color compatible amb HTML per mostrar l'horari
        """
        self.checkBusySeveral(time)
        self.isValid(code, typ, group)
        for t in time:
            weekday, hours = t
            ch = hours[0]
            h = generateHoursFrom(ch)
            while ch != hours[1]:
                ch = next(h)
                self[weekday, ch] = (group, subject, color)

    def unloadSubject(self, code, typ, time):
        """ Esborra les assignatures de l'horari les hores que apareixen a time
        Args:
            code (int) : codi de l'assignatura. Ex 240041
            typ (str) : tipus d'assignatura. Teoria: 't', Pràctiques: 'p'
            time (list) : dies i hores. Ex:
            [(dia de la setmana 1, [hora inici 1, hora final 1]), (ds2, [hi2, hf2])]
        """
        try:
            if typ == 't':
                del self.subjects[code]
        except:
            pass
        for t in time:
            weekday = t[0]
            hours = t[1]
            ch = hours[0]
            h = generateHoursFrom(ch)
            while ch != hours[1]:
                ch = next(h)
                if self.isBusy(weekday, ch):
                    del self.data[weekday][ch]

    def saveTimeTable(self, filename):
        """ Guarda l'horari generat
        Args:
            filename (str) : nom del fitxer.
        """
        with open(str(filename)+'.html', 'w') as save:
            save.write(str(self))