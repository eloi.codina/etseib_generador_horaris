"""
Dades extretes de:
https://etseib.upc.edu/ca/estudis/graus/horaris-del-geti
   
Atenció: poden haver-hi errors

Si no apareixen les que voleu cursar, podeu afegir assignatures, mantenint 
l'esquema dels diccionaris que ja hi ha.

{'name':'nom_assignatura',
  'code':codi_únic_assignatura, #(per exemple, el codi intern de l'UPC)
  'type':'teoria_o_pràctiques (t/p)',
  'color':color[i],             #(1<=i<=6, és el color que apareixerà a la 
                                #taula i que podeu modificar al diccionari `color`)
  'groups':{num_grup1:[(dia_setmana1,[hora_inici1,hora_final1-30minuts])
                      (ds2,[hi2, hf2-30minuts])]
            num_grup2:[(ds3,[hi3,hf3-30minuts])]
            }
}
dia_setmana => 1:dilluns, ..., 5:divendres
hores => 800,830,900,930,1000,1030, ..., 2330 (intervals de mitja hora)

20/7/2017
(C) 2017 Eloi Codina
"""
############# COLORS TAULA ##############
color =  {1:'AliceBlue',
          2:'SkyBlue',
          3:'yellow',
          4:'Chartreuse',
          5:'PaleGoldenRod ',
          6:'DarkSalmon'}
#########################################

############## OBLIGATORI ###############
T240051 =     {'name':'Tecnologia del Medi Ambient i Sostenibilitat',
              'code':240051,
              'type':'t',
              'color':color[1],
              'groups':{10:[(3,[1200,1330]), (5,[1200,1330])],
              20:[(1,[1000,1130]), (4,[1000,1130])],
              30:[(2,[800,930]), (4,[800,930])],
              40:[(1,[1600,1730]), (3,[1600,1730])],
              50:[(1,[1800,1930]), (3,[1800,1930])]
              }}
T240052 =     {'name':'Termodinàmica T',
              'code':240052,
              'type':'t',
              'color':color[2],
              'groups':{10:[(2,[1000,1130]), (4,[1000,1130])],
              20:[(2,[800,930]), (4,[800,930])],
              30:[(1,[800,930]), (3,[800,930])],
              40:[(1,[1400,1530]), (5,[1400,1530])],
              50:[(1,[1600,1730]), (3,[1600,1730])]
              }}
P240052 =     {'name':'Termodinàmica L',
              'code':240052,
              'type':'p',
              'color':color[2],
              'groups':{11:[(1,[1400,1530])],
              12:[(2,[1400,1530])],
              13:[(3,[1400,1530])],
              14:[(4,[1400,1530])],
              21:[(1,[1600,1730])],
              22:[(2,[1600,1730])],
              23:[(3,[1600,1730])],
              24:[(4,[1600,1730])],
              31:[(1,[1600,1730])],
              32:[(2,[1600,1730])],
              33:[(3,[1600,1730])],
              34:[(4,[1600,1730])],
              41:[(1,[1000,1130])],
              42:[(2,[1000,1130])],
              43:[(3,[1000,1130])],
              44:[(4,[1000,1130])],
              51:[(1,[1000,1130])],
              52:[(2,[1000,1130])],
              53:[(3,[1000,1130])],
              54:[(4,[1000,1130])]
              }}
T240053 =     {'name':'Electrotècnia T',
              'code':240053,
              'type':'t',
              'color':color[3],
              'groups':{10:[(1,[1000,1130]), (3,[1000,1130])],
              20:[(1,[800,930]), (3,[800,930])],
              30:[(2,[1000,1130]), (4,[1000,1130])],
              40:[(2,[1400,1530]), (4,[1400,1530])],
              50:[(2,[1600,1730]), (4,[1600,1730])]
              }}
P240053 =     {'name':'Electrotècnia L',
              'code':240053,
              'type':'p',
              'color':color[3],
              'groups':{11:[(1,[1600,1730])],
              12:[(1,[1400,1530])],
              13:[(2,[1400,1530])],
              14:[(2,[1600,1730])],
              21:[(4,[1600,1730])],
              22:[(4,[1800,1930])],
              23:[(2,[1800,1930])],
              24:[(3,[1600,1730])],
              31_32:[(3,[1600,1730])],
              33:[(2,[1400,1530])],
              34:[(3,[1400,1530])],
              41:[(2,[1000,1130])],
              42:[(2,[1200,1330])],
              43:[(1,[1000,1130])],
              44:[(3,[1000,1130])],
              51:[(5,[1000,1130])],
              52:[(1,[1200,1330])],
              53:[(3,[1200,1330])],
              54:[(5,[1200,1330])]
              }}
T240054 =     {'name':'Mecànica dels Medis Continus T',
              'code':240054,
              'type':'t',           
              'color':color[4],
              'groups':{10:[(2,[830,930]), (5,[830,930])],
              20:[(2,[1000,1100]), (5,[1000,1100])],
              30:[(1,[1000,1100]), (3,[1000,1100])],
              40:[(2,[1600,1700]), (4,[1600,1700])],
              50:[(1,[1430,1530]), (3,[1430,1530])]
              }}
P240054 =     {'name':'Mecànica dels Medis Continus L',
              'code':240054,
              'type':'p',
              'color':color[4],
              'groups':{11:[(3,[1800,1930])],
              12:[(4,[1600,1730])],
              13:[(4,[1800,1930])],
              14:[(5,[1600,1730])],
              21:[(5,[1600,1730])],
              22:[(3,[1800,1930])],
              23:[(5,[800,930])],
              24:[(5,[1400,1530])],
              31:[(5,[800,930])],
              32:[(5,[1000,1130])],
              33:[(4,[1600,1730])],
              34:[(4,[1800,1930])],
              41:[(5,[1200,1330])],
              42:[(1,[800,930])],
              43:[(5,[1200,1330])],
              51_53:[(5,[1800,1930])],
              52:[(5,[1000,1130])]
              }}
T240055 =     {'name':'Tècniques Estadístiques per a la Qualitat',
              'code':240055,
              'type':'t',
              'color':color[5],
              'groups':{10:[(1,[800,930])],
              20:[(3,[1200,1330])],
              30:[(2,[1200,1330])],
              40:[(3,[1400,1530])],
              50:[(5,[1400,1530])]
              }}
T240151 =     {'name':'Tecnologia i Selecció de Materials T',
              'code':240151,
              'type':'t',           
              'color':color[6],
              'groups':{10:[(4,[830,930]), (5,[1000,1100])],
              20:[(3,[1000,1100]), (5,[1130,1230])],
              30:[(3,[1200,1300]), (1,[1200,1300])],
              40:[(2,[1730,1830]), (4,[1730,1830])],
              50:[(2,[1430,1530]), (4,[1430,1530])]
              }}
P240151 =     {'name':'Tecnologia i Selecció de Materials L',
              'code':240151,
              'type':'p',
              'color':color[6],
              'groups':{11:[(3,[800,930])],
              12:[(3,[800,930]), (4,[800,930])],
              13:[(3,[800,930]), (5,[1000,1130])],
              21:[(2,[1200,1330])],
              22:[(2,[1200,1330]), (3,[1000,1130])],
              23:[(2,[1200,1330]), (5,[1200,1330])],
              31:[(3,[1200,1330]), (5,[800,930])],
              32:[(3,[1400,1530]), (5,[800,930])],
              33:[(5,[800,930])],
              41:[(2,[1730,1900]), (5,[1800,1930])],
              42:[(4,[1730,1900]), (5,[1800,1930])],
              43:[(5,[1800,1930])],
              51:[(2,[1400,1530]), (5,[1600,1730])],
              52:[(4,[1400,1530]), (5,[1600,1730])],
              53:[(5,[1600,1730])]
              }}
#########################################
