Aquest conjunt d'scripts genera totes les combinacions d'horaris sense solapaments
per les assignatures del quadrimestre de tardor del tercer curs (Q5) de l'ETSEIB
(UPC). Es pot utilitzar per qualsevol quadrimestre, però si no és el 5è, haureu 
d'afegir les dades manualment. <b>Actualitzat pel curs 2017-2018.</b><br />

Primer, descarregueu tots els fitxers a la vostra màquina (Windows/Mac/Linux) on
heu de tenir instal·lat un intèrpret de Python 3.X.<br />

Com podeu veure al fitxer dades.py, l'script tracta la part de teoria i de 
pràctica d'una assignatura com a assignatures diferents. A part, si hi ha més 
d'un subgrup (ex 11,12,13) que té exactament el mateix horari, només en deixa un, 
per tal d'evitar moltes opcions repetides.<br /><br />


Per exectuar aquest script i obtenir totes les possibles combinacions d'horaris:<br />

<b>1)</b> Obrir el fitxer `dades.py` i apuntar-se el nom de les variables de
   les assignatures que es volen cursar. <br />
   Veureu que teoria i pràctica d'una mateixa matèria compten com a assignatures diferents. <br />
<b>2)</b> Obrir el fitxer `combinacions.py` i canviar el valor de les variables `Si` i>=1 per
   les variables de les assignatures que s'han escollit.<br />
   NOTA: probablament, només canviareu S1 i S2 (optativa i projecte, respectivament),
   ja que les altres són assignatures obligatòries.<br />
<b>3)</b> Modificar la variable `assignatures` si no heu seleccionat 9 assignatures.<br />
   ATENCIÓ: la llista és ordenada. Cal que, si una assignatura té teoria i pràctica,
   primer poseu la part de teoria i després la de pràctica.<br />
<b>4)</b> Modificar la variable `un_unic_html` (booleà). Si poseu `True`, es generarà un
   únic fitxer HTML amb totes les combinacions. Amb `False`, es generaran tants
   fitxers com combinacions hi hagi.<br />
<b>5)</b> Si `un_unic_html=True`, modifiqueu la variable `nom_fitxer` per escollir el nom del 
   fitxer on voleu que es guardin les combinacions. Ha d'acabar amb `.html`. Si voleu que es guardi en una altra
   carpeta, assegureu-vos que el camí existeix.<br />
   Altrament, modifiqueu la variable `carpeta_desa` per escollir la carpeta on es vol 
   que es guardin totes les opcions sense solapaments.<br />
   ATENCIÓ: creeu la carpeta abans d'executar el programa. La variable ha 
   d'acabar amb `/`.<br />
<b>6)</b> Executar `combinacions.py` amb un intèrpret de Python. S'ha provat amb la versió
   3.6, però hauria d'anar amb totes les 3.X<br />
<b>7)</b> Podreu veure diversos fitxers HTML a la carpeta `carpeta_desa` si així ho heu seleccionat. El nom del fitxer
   correspon al timestamp de quan s'ha generat, per tal d'evitar que se solapin.<br />
   Altrament, els podreu veure tots junts al fitxer `nom_fitxer`.
   <br />
   <br />
Darrera modificació: 20/7/2017
   <br />
   <br />
(C) 2017 - Eloi Codina