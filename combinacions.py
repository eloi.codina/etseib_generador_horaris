"""
Programa principal, crea totes les combinacions possibles de les 6 assignatures
del Q4 de l'ETSEIB. Hi ha 9 variables perquè hi ha assignatures on hi ha part
de teoria i part de laboratori / pràctica.

29/1/2017
(C) 2017 Eloi Codina
"""

from dades import *
from timetable import *
from time import time, sleep

un_unic_html= True
#Fitxer on es guardaran totes les combinacions si `un_unic_html` == True
nom_fitxer = 'combinacions.html'
#Carpeta on es guardaran les combinacions en fitxers separats si `un_unic_html` == False
carpeta_desa = 'combinacions/' #Si es vol que les combinacions estiguin en fitxers separats


# Assignar cada variable a una optativa/projecte/trocal teoria/troncal pràctiques
# del fitxer dades.py.
# Es pot eliminar o afegir variables.
S1 = T240407
S2 = P240044_3
S3 = T240041
S4 = T240042
S5 = P240042
S6 = T240043
S7 = P240043
S8 = T240141
S9 = P240141

# En aquesta llista han d'aparèixer totes les variables anteriors de les que es
# vol calcular combinacions d'horari.
# MOLT IMPORTANT: si una assignatura té grup de pràctiques i de teoria, primer
#           ha d'aparèixer el grup de teoria i despres el de pràctiques.
assignatures = [S1,S2,S3,S4,S5,S6,S7,S8,S9]

# Aquí es guardaran totes les taules HTML
totes_combinacions = []


def allCombinations(sub, t, i, at):
    """ Genera totes les possibles combinacions d'horaris i les guarda a `t`.
    Per cada combinació generada, es guarda la taula HTML associada a `t` a `at`.
    Atenció: `at` ha de correspondre a una variable de fora la funció, ja que 
            es modifica cada cop que es genera una combinació.
        
    Args:
        sub (list) : llista de les assignatures que es volen cursar
        t (TimeTable) : objecte de tipus timetable.TimeTable. Inicialment buit
        i (int) : posició a la llista `sub`. Inicialment i=0
        at (list) : llista on es guardaran totes les taules HTML generades.
    """
    for g in sub[i]['groups']:
        try:
            t.loadSubject(sub[i]['name'], sub[i]['code'], sub[i]['type'], g, 
                          sub[i]['groups'][g],sub[i]['color'])
        except (HourBusy,GroupNotValid):
            # L'hora ja està ocupada o el grup de pràctiques no és correcte
            continue
        if i == len(sub)-1: # Estem a l'últim element de la llista `sub`
            if un_unic_html:
                at.append(str(t)) # Ja tenim una combinació, guardem la taula HTML
            else:
                t.saveTimeTable(carpeta_desa+str(time()))
            sleep(0.02)
        else: # Encara queden elements a la llista `sub`. Crida recursiva
            allCombinations(sub, t, i+1,at)
        # Eliminem el grup que acabem de posar per poder posar el següent
        t.unloadSubject(sub[i]['code'], sub[i]['type'], sub[i]['groups'][g])

allCombinations(assignatures, TimeTable(), 0, totes_combinacions)

if un_unic_html:
    with open(nom_fitxer,'w') as html:
        html.write('<br><br>'.join(totes_combinacions))